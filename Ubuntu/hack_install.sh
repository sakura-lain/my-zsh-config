#!/bin/bash

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.0.0/Hack.zip
mkdir ../hack
unzip Hack.zip -d ../hack
mv ../hack /usr/share/fonts/
fc-cache -f -v #recharge le cache des polices
#fc-list | grep "Hack" #vérification
#rm -rf Hack* #Remove the files Hack* from the repository folder
#rm -rf ../hack
