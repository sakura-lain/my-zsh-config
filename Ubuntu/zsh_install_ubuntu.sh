#!/bin/bash

#installation de zsh
sudo apt install -y zsh-doc
#sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sudo apt install -y zsh

#installation des plugins
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git  ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions
#git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
sudo apt install -y thefuck autojump

#polices de caractères
sudo apt install -y fonts-powerline
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.0.0/Hack.zip
mkdir hack
unzip Hack.zip -d hack
sudo mv ~/hack /usr/share/fonts/truetype/
sudo fc-cache -f -v #recharge le cache des polices
#fc-list | grep "Hack" #vérification
rm -rf Hack* #Remove the files Hack* from the repository folder
rm -rf hack

#pyenv
sudo apt install -y pyenv

#création du répertoire pour les fichiers de configuration de la complétion
mkdir .zsh/completion

#colorls
sudo apt install -y ruby ruby-dev rubygems libncurses5-dev
sudo gem install colorls

#linuxlogo
sudo apt install -y linuxlogo

#fichier de config
#cp .zshrc ~/.zshrc
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
#p10k configure
