#!/bin/bash
# To be run as normal user

sudo ln -s $HOME/.oh-my-zsh           /root/.oh-my-zsh
sudo ln -s $HOME/.zshrc               /root/.zshrc
sudo ln -s $HOME/.p10k.zsh            /root/.p10k.zsh
sudo ln -s $HOME/.zsh_aliases         /root/.zsh_aliases
