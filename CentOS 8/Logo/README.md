# Steps to build a custom logo for linux_logo

## Install Cacaview

- Intall `cacaview` (useful to previsualize your file):
```
sudo apt install caca-utils
```
- Usage : `cacaview yourfile.png`

## Convert your image file to ASCII text

```
img2txt yourfile.png > yourfile.txt
```
If the result is to large, you can dapt its width (default: 60):
```
img2txt -W 30 yourfile.png > yourfile.txt
```

## Convert your text file to CentOS_logo_small_ansi
```
iconv -f utf8 -t cp1252 yourfile.txt > yourfile.logo
```

## Add lines to complete your logo
Use the models provided in the `logos` directory of linux_logo project. The file should present as follow:

```
#Optional comments

SYSINFO_POSITION bottom #Position of system information : left, right, bottom
NAME centos #Logo name

DESCRIPTION_STRING CentOS Logo #Logo description

BEGIN_LOGO
# ANSI code
END_LOGO

BEGIN_ASCII_LOGO #Optional
#ASCII logo (opional)
END_ASCII_LOGO #Optional

```

## Display your logo with linux_logo
```
linuxlogo -D yourfile.logo #Ubuntu
linux_logo -D yourfile.logo #CentOS
```

## Display in ZSH
Add step is to add this command, with the correct path to your logo, to your `.zshrc` file.

## Sources
- https://memo-linux.com/ascii-art-sous-linux/
- https://github.com/deater/linux_logo/blob/master/README.CUSTOM_LOGOS
- http://www.laurentdelaunoy.com/blog/encodage-utf8.php
