#!/bin/bash

git clone https://github.com/deater/linux_logo.git
cd linux_logo
find ./logos -type f | sort > logo_config
./configure
make
sudo make install
