## My Zsh config

Here's a copy of my zsh config, feel free to use it!

### Installation and configuration

There are installation scripts for both Debian/Ubuntu (tested on Ubuntu 20.04) and CentOS 8, as well as `.zshrc` custom templates files. There are also configuration files common to both files systems.

- Run the installation scripts.
- Change your default shell for ZSH using the provided script.
- Optional : configure Powerlevel10k with `p10k configure` (not necessary if you use the provided `p10k.zsh` file).
- Copy the configuration files to your home directory. With Powerlevel10k, at least `.p10k.zsh` and `.zshrc` should be copied.
- En CentOS, you can copy the customised logo provided [here](./CentOS 8/Logo/CentOS_logo_small_ansi.logo) to your home directory under the name `centos.logo`. To know more about how to create your own logos, please see [here](./CentOS 8/Logo/README.md).
- Enable "Hack Nerd Font Regular 10" in Terminator, "Hack Regular" in Gnome Terminal or "Meslo LG S for Powerline 11pt" in Konsole.
- To install for root, use the dedicated script to create symlinks from the configuration files contained in your home directory to the one of root. You will be prompted to update Oh My ZSH next time you connect with the root user. Don't forget to change root's default shell.
- To load or reload the configuration, either connect to a new shell or, more conveniently, use the following command:
```
source .zshrc
```

#### Powerlevel10k configuration

Here are my choices and remarks for `p10k configure`:

- The rotated square should look like a diamond if "Hack Nerd Font Regular 10" is enabled in your terminal.
- The lock should look like a lock.
- The Debian logo should look like a Debian logo.
- If icons don't overlap, choose "yes", otherwise choose "no".
- Choose "Rainbow" as prompt style;
- Choose "unicode" as character set;
- Choose a 24-hour format to display current time;
- Choose the angle separator as prompt separator;
- Choose "blurred" as prompt head style;
- Choose "flat" as prompt tail ("blurred" is also a good option);
- Choose "two lines" as prompt height;
- Choose "disconnected" as prompt connection;
- Choose "left" as prompt frame;
- Choose "lightest" as prompt color;
- Choose "sparse" as prompt spacing;
- Choose "many icons" as icons display style;
- Choose "concise" as prompt flow;
- Enable transient prompt;
- Choose "verbose" as instant prompt mode;
- Finally overwrite your `.p10k.zsh` to save changes.

Note : the configuration can be even more customized while using `POWERLEVEL9K_` variables in your `.zshrc` file. Thoses are fully compatible with Powerlevel10k.

### Plugins and appearance

Here are the main appearence and shell improvements plugins I use (you can see a more detailed list in .zshrc files):

- autojump
- colored-man-pages
- command-not-found
- common-aliases (with neutralisation of aliases for cp, mv and rm)
- debian (for Debian and its derivatives)
- sudo
- systemd
- thefuck
- z
- zsh-autosuggestions
- zsh-syntax-highlighting

I also use linuxlogo to display a logo with system informations at the terminal opening, and `colorls` to colorize ls. For CentOS, I added an alias, `lZ` to display SeLinux context using Bash, since the `ls -Z` option remains unknown from `colorls`.

Three screenshots using Terminator with font hack nerd font regular 10 and Powerlevel9k:

![](screenshots/sakura_zsh_fullscreen.png "Full screen rendering")*<br/>Full screen rendering*

![](screenshots/sakura_zsh_window.png "Windowed rendering")*<br/>Windowed rendering*

![](screenshots/sakura_zsh_colorls.png "With colorls")*<br/>With `colorls`*

Two screenshots using Terminator with font hack nerd font regular 10 and Powerlevel10k, showing my CentOS custom logo:
![](screenshots/CentOS_ZSH_p10k_small.png "Windowed rendering")*<br/>Windowed rendering, showing path abbreviation.*
![](screenshots/CentOS_ZSH_p10k_big.png "Full screen rendering")*<br/>Full screen rendering, showing transient promt.*

### sources

- https://jdhao.github.io/2018/10/13/centos_zsh_install_use/
- https://mpolinowski.github.io/installing-oh-my-zsh-on-cent-os-8
- https://medium.com/@alex285/get-powerlevel9k-the-most-cool-linux-shell-ever-1c38516b0caa
- https://github.com/Powerlevel9k/powerlevel9k/wiki/Install-Instructions#option-2-install-for-oh-my-zsh
- https://github.com/romkatv/powerlevel10k
- https://github.com/romkatv/powerlevel10k/blob/master/README.md#meslo-nerd-font-patched-for-powerlevel10k
- https://github.com/source-foundry/Hack
- https://github.com/deater/linux_logo
- https://www.tecmint.com/linux_logo-tool-to-print-color-ansi-logos-of-linux/
- https://www.system-linux.eu/index.php?post/2009/01/05/Joli-logo-au-demarrage-dun-shell
- https://github.com/romkatv/powerlevel10k/blob/master/README.md#instant-prompt
- https://github.com/romkatv/powerlevel10k/blob/master/README.md#how-do-i-enable-instant-prompt
- https://awesomeopensource.com/project/romkatv/powerlevel10k
- https://github.com/denysdovhan/spaceship-prompt
- https://github.com/romkatv/powerlevel10k/issues/170
- https://github.com/romkatv/powerlevel10k/issues/455
- https://www.reddit.com/r/zsh/comments/euoox8/how_do_i_run_p10k_configure/
- https://github.com/ohmyzsh/ohmyzsh/issues/7986
- https://gist.github.com/goyalankit/a1c88bfc69107f93cda1

### See also

- [My laptop config](https://gitlab.com/sakura-lain/my-laptop-config)
- [My CentOS 8 desktop installation for a professional use](https://gitlab.com/sakura-lain/my-centos8-desktop-professional/-/blob/master/README.md)
